export interface Resultat {
code: string,
nom : string,
date : string,
casConfirmes : number,
hospitalises : number,
reanimation : number,
nouvellesHospitalisations : number,
nouvellesReanimations : number,
deces : number,
gueris : number,
source : Source;
}

export interface Source{

nom : string;

}