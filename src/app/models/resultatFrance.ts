export interface ResultatFrance {
    date : string;
    casConfirmes: number,
    deces : number,
    decesEhpad : number,
    hospitalises : number,
    reanimation : number,
    nouvellesHospitalisations : number,
    nouvellesReanimations : number,
 code : string,
 nom : string;
    gueris : number,
    casConfirmesEhpad : number,

    }
   