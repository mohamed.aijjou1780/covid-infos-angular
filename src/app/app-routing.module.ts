import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AfficheJourComponent } from './pages/affiche-jour/affiche-jour.component';
import { AfficheSemaineComponent } from './pages/affiche-semaine/affiche-semaine.component';
import { AfficheMoisComponent } from './pages/affiche-mois/affiche-mois.component';
import { ErrorComponent } from './pages/error/error.component';
import { RegionComponent } from './pages/region/region.component';

const routes: Routes = [
{path: 'jour', component : AfficheJourComponent},
{path : 'error', component : ErrorComponent},
{path: 'semaine', component : AfficheSemaineComponent},
{path: 'region/:code', component : RegionComponent},
{path : 'mois', component : AfficheMoisComponent},
{path : '', component : AfficheJourComponent},
{path : '**', component : AfficheJourComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
