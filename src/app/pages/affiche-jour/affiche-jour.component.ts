import { Component, OnInit, ViewChild } from '@angular/core';
import { AfficheServiceService } from 'src/app/services/affiche-service.service';
import { Resultat } from 'src/app/models/resultat';
import { formatDate, DatePipe } from '@angular/common';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ResultatFrance } from 'src/app/models/resultatFrance';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { faVirus } from '@fortawesome/free-solid-svg-icons';
import { faHospital } from '@fortawesome/free-solid-svg-icons';
import { faMale } from '@fortawesome/free-solid-svg-icons';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { faPercentage } from '@fortawesome/free-solid-svg-icons';
import { faProcedures } from '@fortawesome/free-solid-svg-icons';
import { faVirusSlash } from '@fortawesome/free-solid-svg-icons';
import { faChartBar } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { finalize } from 'rxjs/operators';
import { AnyMxRecord, AnyNaptrRecord } from 'dns';


@Component({
  selector: 'app-affiche-jour',
  templateUrl: './affiche-jour.component.html',
  styleUrls: ['./affiche-jour.component.scss'],
  providers: [DatePipe]
})
export class AfficheJourComponent implements OnInit {
  date: string;
  resultat: Resultat[];
  resultatVeille: Resultat[];
  resultatRegion: Resultat[];
  result: Resultat;
  config: any;
  deces: number;
  decesVeille: number;
  rows = [];
  temp = [];
  resultatFrance: any;
  resultatFranceVeille: any;
  resultatFranceModif: any;
  resultatFranceVeilleModif: any;
  faCoffee = faCoffee;
  faVirus = faVirus;
  faHospital = faHospital;
  faMale = faMale;
  faUserPlus = faUserPlus;
  faPercentage = faPercentage;
  faProcedures = faProcedures;
  faVirusSlash = faVirusSlash;
  faChartBar = faChartBar;
  faInfoCircle = faInfoCircle;
  progHospi: any;
  progConta: any;
  diffConta: any;
  infosDeces: boolean;
  infosDecesVeille: boolean;
  diffDeces: any;
  progDeces: any;
  decesAvecEhpad = undefined;
  decesVeilleAvecEhpad: number;
  showSpinner = true;
  diffGueris: any;
  options: any;
  panelOpenState = false;
  guerrison: number;
  reanimations: number;
  nouvellesReanimations: number;
  hospitalisation: number;
  nouvellesHospitalisation: number;
  initOpts = {
    renderer: 'svg',
    width: 600,
    height: 400,
  };

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private serviceResultat: AfficheServiceService,
    private datePipe: DatePipe, private router: Router, private spinner: NgxSpinnerService
  ) { }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nom.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  ngOnInit(): void {
    this.spinner.show();
    this.creationGraphique();
    const datec = new Date();
    console.log(datec);
    let dateVeille;
    let infos;
    let dateAvantVeille;
    const formattedDate = this.datePipe.transform(datec, 'yyyy-MM-dd');
    console.log(formattedDate);
    console.log(datec.getHours());
    if (datec.getHours() < 21) {
      infos = true;
    } else {
      infos = false;
    }
    console.log(infos);
    if (datec.getHours() < 21) {
      if (datec.getDate() < 10 && (datec.getMonth() + 1) < 10) {
        if (datec.getDate() == 1 && (datec.getMonth() + 1) <= 8 || datec.getDate() == 1 && (datec.getMonth() + 1) == 9) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 5 || datec.getDate() == 1 && (datec.getMonth() + 1) == 7) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 1) {
          dateVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          dateAvantVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 3) {
          if (datec.getFullYear() % 400 != 0) {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log("bisext");
            dateAvantVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
          } else {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            dateAvantVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            console.log("pas bisext");
          }
        } else if (datec.getDate() != 1) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 2);
        }
      } else if (datec.getDate() > 10 && (datec.getMonth() + 1) >= 10) {
        dateVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' +
          (datec.getDate() - 1);
        dateAvantVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' +
          (datec.getDate() - 2);
      } else if (datec.getDate() == 10 && (datec.getMonth() + 1) >= 10) {
        dateVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' + '0' +
          (datec.getDate() - 1);
        dateAvantVeille =
          datec.getFullYear() +
          '-' + '0' +
          (datec.getMonth() + 1) +
          '-' +
          (datec.getDate() - 2);
      } else if (datec.getDate() < 10 && (datec.getMonth() + 1) >= 10) {
        if (datec.getDate() == 1 && (datec.getMonth() + 1) == 11) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '31';
          dateAvantVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '30';
          console.log("9");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) != 12) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("10");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '29';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) == 12) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '30';
          console.log("11");
          dateAvantVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '29';
        } else if (datec.getDate() != 1 && datec.getDate() != 2) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          dateAvantVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 2);
        } else if (datec.getDate() == 2 && (datec.getMonth() + 1) == 11) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          dateAvantVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth()) +
            '-' + '31';
          console.log('coucou');
        } else if (datec.getDate() == 2 && (datec.getMonth() + 1) == 12) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          dateAvantVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth()) +
            '-' + '30';
        }
      } else if (datec.getDate() >= 10 && (datec.getMonth() + 1) < 10) {

        if (datec.getDate() == 1 && ((datec.getMonth() + 1) % 2 == 0 && datec.getFullYear() % 400 != 0 && (datec.getMonth() + 1) <= 8) || datec.getDate() == 1 && (datec.getMonth() + 1) == 9) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          console.log("16");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 3 == 0 || datec.getDate() == 1 && (datec.getMonth() + 1) == 5 || datec.getDate() == 1 && (datec.getMonth() + 1) == 7) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("17");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '29';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 1) {
          dateVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          console.log("18");
          dateAvantVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '30';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 3) {
          if (datec.getFullYear() % 400 != 0) {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            console.log("19");
            dateAvantVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '27';
          } else {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log("20");
            dateAvantVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
          }
        } else if (datec.getDate() != 10) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' +
            (datec.getDate() - 1);

          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' +
            (datec.getDate() - 2);
        }
        else if (datec.getDate() == 10) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          console.log("22");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 2);
        }

      }
      console.log(dateVeille);
      console.log(dateAvantVeille);
      this.serviceResultat.get2ResultatByDate(dateVeille, dateAvantVeille).subscribe((res: any[]) => {
        this.resultat = res[0].allFranceDataByDate;
        this.resultatVeille = res[1].allFranceDataByDate;

        this.resultatRegion = this.resultat.filter(x => x.nom == "Île-de-France" || x.nom == "Auvergne-Rhône-Alpes" || x.nom == "Hauts-de-France" || x.nom == "Provence-Alpes-Côte d'Azur" || x.nom == "Occitanie" || x.nom == "Grand Est"
          || x.nom == "Nouvelle-Aquitaine" || x.nom == "Normandie" || x.nom == "Bourgogne-Franche-Comté" || x.nom == "Pays de la Loire" || x.nom == "Bretagne" || x.nom == "Corse" || x.nom == "Centre-Val de Loire");

        this.rows = this.resultatRegion;
        this.temp = [...this.resultatRegion];
        this.resultatFranceVeille = this.resultatVeille.find(
          x => x.code == 'FRA' && x.nom == 'France'
        );
        this.resultatFrance = this.resultat.find(
          x => x.code == 'FRA' && x.nom == 'France'
        );

        this.resultatFranceModif = this.resultat.find(x => x.code == 'FRA' && x.nom == 'France' && x.source.nom == 'OpenCOVID19-fr');

        this.resultatFranceVeilleModif = this.resultatVeille.find(x => x.code == 'FRA' && x.nom == 'France' && x.source.nom == 'OpenCOVID19-fr');

        if (this.resultatFrance.hospitalises != undefined && this.resultatFrance.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatFrance.hospitalises;
          this.nouvellesHospitalisation = this.resultatFrance.nouvellesHospitalisations;
        } else if (this.resultatFrance.hospitalises != undefined && this.resultatFrance.nouvellesHospitalisations == undefined) {
          this.hospitalisation = this.resultatFrance.hospitalises;
          this.nouvellesHospitalisation = this.resultatFranceModif.nouvellesHospitalisations;
        } else if (this.resultatFrance.hospitalises == undefined && this.resultatFrance.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatFranceModif.hospitalises;
          this.nouvellesHospitalisation = this.resultatFrance.nouvellesHospitalisations;
          console.log('coucou');
        } else {
          this.hospitalisation = this.resultatFranceModif.hospitalises;;
          this.nouvellesHospitalisation = this.resultatFranceModif.nouvellesHospitalisations;
          console.log(this.hospitalisation);
          console.log(this.nouvellesHospitalisation);

        }

        // if (this.resultatFranceVeille.hospitalises != undefined && this.resultatFranceVeille.nouvellesHospitalisations != undefined) {
        //   this.hospitalisation = this.resultatFranceVeille.hospitalises;
        //   this.nouvellesHospitalisation = this.resultatFranceVeille.nouvellesHospitalisations;
        // } else if (this.resultatFranceVeille.hospitalises != undefined && this.resultatFranceVeille.nouvellesHospitalisations == undefined) {
        //   this.hospitalisation = this.resultatFrance.hospitalises;
        //   this.nouvellesHospitalisation = this.resultatFranceVeilleModif.nouvellesHospitalisations;
        // } else if (this.resultatFranceVeille.hospitalises == undefined && this.resultatFranceVeille.nouvellesHospitalisations != undefined) {
        //   this.hospitalisation = this.resultatFranceVeilleModif.hospitalises;
        //   this.nouvellesHospitalisation = this.resultatFranceVeille.nouvellesHospitalisations;
        // } else {
        //   this.hospitalisation = undefined;
        //   this.nouvellesHospitalisation = undefined;
        // }


        if (this.resultatFrance.deces != undefined) {
          if (this.resultatFrance.decesEhpad != undefined) {
            this.decesAvecEhpad = this.resultatFrance.deces + this.resultatFrance.decesEhpad;
            console.log(this.decesAvecEhpad);

          } else {
            this.deces = this.resultatFrance.deces;

          }
        } else {
          if (this.resultatFranceModif.decesEhpad != undefined) {
            this.decesAvecEhpad = this.resultatFranceModif.deces + this.resultatFranceModif.decesEhpad;
            console.log(this.decesAvecEhpad);

          } else {
            this.deces = this.resultatFranceModif.deces;

          }
        }


        if (this.resultatFranceVeille.deces != undefined) {
          if (this.resultatFranceVeille.decesEhpad != undefined) {
            this.decesVeilleAvecEhpad = this.resultatFranceVeille.deces + this.resultatFranceVeille.decesEhpad;

          } else {
            this.decesVeille = this.resultatFranceVeille.deces;

          }
        } else {
          if (this.resultatFranceVeilleModif.decesEhpad != undefined) {
            this.decesVeilleAvecEhpad = this.resultatFranceVeilleModif.deces + this.resultatFranceVeilleModif.decesEhpad;

          } else {
            this.decesVeille = this.resultatFranceVeilleModif.deces;

          }
        }

        if (this.decesAvecEhpad == undefined || this.decesVeilleAvecEhpad == undefined) {
          this.diffDeces = this.deces - this.decesVeille;

          this.progDeces = ((this.deces - this.decesVeille) / this.decesVeille) * 100;
        } else if (this.decesAvecEhpad != undefined && this.decesVeilleAvecEhpad != undefined) {
          this.diffDeces = this.decesAvecEhpad - this.decesVeilleAvecEhpad;

          this.progDeces = ((this.decesAvecEhpad - this.decesVeilleAvecEhpad) / this.decesVeilleAvecEhpad) * 100;
        }

        if (Number.isNaN(this.diffDeces)) {
          this.diffDeces = false;
        }
        if (Number.isNaN(this.progDeces)) {
          this.progDeces = false;
        }

        if (this.hospitalisation != undefined && this.nouvellesHospitalisation != undefined) {
          this.progHospi =
            (this.nouvellesHospitalisation /
              (this.hospitalisation -
                this.nouvellesHospitalisation)) *
            100;
        } else { this.progHospi = undefined; }

        this.diffConta = this.resultatFrance.casConfirmes - this.resultatFranceVeille.casConfirmes;

        if(Number.isNaN(this.diffConta)){
          this.diffConta == false;
        }

        if (this.resultatFrance.gueris != undefined && this.resultatFranceVeille != undefined) {
          this.guerrison = this.resultatFrance.gueris;
          this.diffGueris = this.resultatFrance.gueris - this.resultatFranceVeille.gueris;
        } else {
          this.guerrison = this.resultatFranceModif.gueris;
          this.diffGueris = this.resultatFranceModif.gueris - this.resultatFranceVeilleModif.gueris;
        }

        if (this.resultatFrance.reanimation != undefined) {
          this.reanimations = this.resultatFrance.reanimation;
          this.nouvellesReanimations = this.resultatFrance.nouvellesReanimations;
        } else {

          this.reanimations = this.resultatFranceModif.reanimation;
          this.nouvellesReanimations = this.resultatFranceModif.nouvellesReanimations;
        }
        this.progConta =
          ((this.resultatFrance.casConfirmes -
            this.resultatFranceVeille.casConfirmes) /
            this.resultatFranceVeille.casConfirmes) *
          100;


          if(Number.isNaN(this.diffGueris)){
            this.diffGueris = false;
          }
          if(Number.isNaN(this.diffConta)){
            this.diffConta = false;
          console.log('non');
          }
          if(Number.isNaN(this.progConta) ){
          console.log('oui');
            this.progConta = false;
          }

          if(Number.isNaN(this.progHospi)  || Number.isNaN(this.progHospi) ){
            this.progHospi = false; 
          }

          console.log(this.diffConta);
          console.log(this.progConta);
          console.log(this.progHospi);


      });
    } else {
      if (datec.getDate() < 10 && (datec.getMonth() + 1) < 10) {
        if (datec.getDate() == 1 && ((datec.getMonth() + 1) % 2 == 0 && datec.getFullYear() % 400 != 0 && (datec.getMonth() + 1) <= 8) || datec.getDate() == 1 && (datec.getMonth() + 1) == 9) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          console.log("1");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 5 || datec.getDate() == 1 && (datec.getMonth() + 1) == 7) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("2");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 1) {
          dateVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          console.log("3");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 3) {
          if (datec.getFullYear() % 400 != 0) {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log("4");
          } else {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            console.log("5");
          }
        } else if (datec.getDate() != 1) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          console.log("6");
        }
        console.log("7");
      } else if (datec.getDate() > 10 && (datec.getMonth() + 1) >= 10) {
        dateVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' +
          (datec.getDate() - 1);
        console.log("8");
      } else if (datec.getDate() == 10 && (datec.getMonth() + 1) >= 10) {
        dateVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' + '0' +
          (datec.getDate() - 1);
        console.log("88");
      } else if (datec.getDate() < 10 && (datec.getMonth() + 1) >= 10) {
        if (datec.getDate() == 1 && (datec.getMonth() + 1) == 11) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '31';
          console.log("9");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) != 12) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("10");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) == 12) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '30';
          console.log("11");
        } else if (datec.getDate() != 1) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          console.log("15");
        }
      } else if (datec.getDate() >= 10 && (datec.getMonth() + 1) < 10) {

        if (datec.getDate() == 1 && ((datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) <= 8) || datec.getDate() == 1 && (datec.getMonth() + 1) == 9) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          console.log("16");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 3 == 0 || datec.getDate() == 1 && (datec.getMonth() + 1) == 5 || datec.getDate() == 1 && (datec.getMonth() + 1) == 7) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("17");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 1) {
          dateVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          console.log("18");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 3) {
          if (datec.getFullYear() % 400 != 0) {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            console.log("19");
          } else {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log("20");
          }
        } else if (datec.getDate() != 10) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' +
            (datec.getDate() - 1);
          console.log("21");
        }
        else if (datec.getDate() == 10) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          console.log("22");
        }
        console.log("23");
      }
      console.log(dateVeille);
      console.log(formattedDate);
      this.serviceResultat.get2ResultatByDate(formattedDate, dateVeille).subscribe((res: any[]) => {

        this.resultat = res[0].allFranceDataByDate;
        this.resultatVeille = res[1].allFranceDataByDate;
        console.log(this.resultat);
        console.log(this.resultatVeille);
        this.resultatRegion = this.resultat.filter(x => x.nom == "Île-de-France" || x.nom == "Auvergne-Rhône-Alpes" || x.nom == "Hauts-de-France" || x.nom == "Provence-Alpes-Côte d'Azur" || x.nom == "Occitanie" || x.nom == "Grand Est"
          || x.nom == "Nouvelle-Aquitaine" || x.nom == "Normandie" || x.nom == "Bourgogne-Franche-Comté" || x.nom == "Pays de la Loire" || x.nom == "Bretagne" || x.nom == "Corse" || x.nom == "Centre-Val de Loire");
        console.log(this.resultatRegion);
        this.rows = this.resultatRegion;
        this.temp = [...this.resultatRegion];
        console.log(this.rows);
        console.log(this.temp);
        this.resultatFranceVeille = this.resultatVeille.find(
          x => x.code == 'FRA' && x.nom == 'France'
        );
        this.resultatFrance = this.resultat.find(
          x => x.code == 'FRA' && x.nom == 'France'
        );
        console.log(this.resultatFranceVeille);
        console.log(this.resultatFrance);
        this.resultatFranceModif = this.resultat.find(x => x.code == 'FRA' && x.nom == 'France' && x.source.nom == 'OpenCOVID19-fr');

        this.resultatFranceVeilleModif = this.resultatVeille.find(x => x.code == 'FRA' && x.nom == 'France' && x.source.nom == 'OpenCOVID19-fr');

        console.log(this.resultatFranceModif);
        console.log(this.resultatFranceVeilleModif);
        if (this.resultatFrance.hospitalises != undefined && this.resultatFrance.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatFrance.hospitalises;
          this.nouvellesHospitalisation = this.resultatFrance.nouvellesHospitalisations;
        } else if (this.resultatFrance.hospitalises != undefined && this.resultatFrance.nouvellesHospitalisations == undefined) {
          this.hospitalisation = this.resultatFrance.hospitalises;
          this.nouvellesHospitalisation = this.resultatFranceModif.nouvellesHospitalisations;
        } else if (this.resultatFrance.hospitalises == undefined && this.resultatFrance.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatFranceModif.hospitalises;
          this.nouvellesHospitalisation = this.resultatFrance.nouvellesHospitalisations;
          console.log('coucou');
        } else {
          this.hospitalisation = this.resultatFranceModif.hospitalises;;
          this.nouvellesHospitalisation = this.resultatFranceModif.nouvellesHospitalisations;
          console.log(this.hospitalisation);
          console.log(this.nouvellesHospitalisation);

        }

        // if (this.resultatFranceVeille.hospitalises != undefined && this.resultatFranceVeille.nouvellesHospitalisations != undefined) {
        //   this.hospitalisation = this.resultatFranceVeille.hospitalises;
        //   this.nouvellesHospitalisation = this.resultatFranceVeille.nouvellesHospitalisations;
        // } else if (this.resultatFranceVeille.hospitalises != undefined && this.resultatFranceVeille.nouvellesHospitalisations == undefined) {
        //   this.hospitalisation = this.resultatFrance.hospitalises;
        //   this.nouvellesHospitalisation = this.resultatFranceVeilleModif.nouvellesHospitalisations;
        // } else if (this.resultatFranceVeille.hospitalises == undefined && this.resultatFranceVeille.nouvellesHospitalisations != undefined) {
        //   this.hospitalisation = this.resultatFranceVeilleModif.hospitalises;
        //   this.nouvellesHospitalisation = this.resultatFranceVeille.nouvellesHospitalisations;
        // } else {
        //   this.hospitalisation = undefined;
        //   this.nouvellesHospitalisation = undefined;
        // }

        if (this.resultatFrance.deces != undefined) {
          if (this.resultatFrance.decesEhpad != undefined) {
            this.decesAvecEhpad = this.resultatFrance.deces + this.resultatFrance.decesEhpad;
            console.log(this.decesAvecEhpad);
            this.deces = undefined;
          } else {
            this.deces = this.resultatFrance.deces;
            this.decesAvecEhpad = undefined;
          }
        } else {
          if (this.resultatFranceModif.decesEhpad != undefined) {
            this.decesAvecEhpad = this.resultatFranceModif.deces + this.resultatFranceModif.decesEhpad;
            console.log(this.decesAvecEhpad);
            this.deces = undefined;
          } else {
            this.deces = this.resultatFranceModif.deces;
            this.decesAvecEhpad = undefined;
          }
        }


        if (this.resultatFranceVeille.deces != undefined) {
          if (this.resultatFranceVeille.decesEhpad != undefined) {
            this.decesVeilleAvecEhpad = this.resultatFranceVeille.deces + this.resultatFranceVeille.decesEhpad;
            this.decesVeille = undefined;
          } else {
            this.decesVeille = this.resultatFranceVeille.deces;
            this.decesVeilleAvecEhpad = undefined;
          }
        } else {
          if (this.resultatFranceVeilleModif.decesEhpad != undefined) {
            this.decesVeilleAvecEhpad = this.resultatFranceVeilleModif.deces + this.resultatFranceVeilleModif.decesEhpad;
            this.decesVeille = undefined;
          } else {
            this.decesVeille = this.resultatFranceVeilleModif.deces;
            this.decesVeilleAvecEhpad = undefined;
          }
        }

        console.log(this.deces);
        console.log(this.decesVeille);
        console.log(this.decesAvecEhpad);
        console.log(this.decesVeilleAvecEhpad);

        if (this.decesAvecEhpad == undefined || this.decesVeilleAvecEhpad == undefined) {
          this.diffDeces = this.deces - this.decesVeille;

          this.progDeces = ((this.deces - this.decesVeille) / this.decesVeille) * 100;
        } else if (this.decesAvecEhpad != undefined && this.decesVeilleAvecEhpad != undefined) {
          this.diffDeces = this.decesAvecEhpad - this.decesVeilleAvecEhpad;

          this.progDeces = ((this.decesAvecEhpad - this.decesVeilleAvecEhpad) / this.decesVeilleAvecEhpad) * 100;
        }

        if (Number.isNaN(this.diffDeces)) {
          this.diffDeces = false;
        }
        if (Number.isNaN(this.progDeces)) {
          this.progDeces = false;
        }
        console.log(this.diffDeces);
        console.log(this.progDeces);

        if (this.hospitalisation != undefined && this.nouvellesHospitalisation != undefined) {
          this.progHospi =
            (this.nouvellesHospitalisation /
              (this.hospitalisation -
                this.nouvellesHospitalisation)) *
            100;
        } else { this.progHospi = undefined; }


        this.diffConta = this.resultatFrance.casConfirmes - this.resultatFranceVeille.casConfirmes;

        if (this.resultatFrance.gueris != undefined && this.resultatFranceVeille != undefined) {
          this.guerrison = this.resultatFrance.gueris;
          this.diffGueris = this.resultatFrance.gueris - this.resultatFranceVeille.gueris;
        } else {
          this.guerrison = this.resultatFranceModif.gueris;
          this.diffGueris = this.resultatFranceModif.gueris - this.resultatFranceVeilleModif.gueris;
        }
        if (this.resultatFrance.reanimation != undefined) {
          this.reanimations = this.resultatFrance.reanimation;
          this.nouvellesReanimations = this.resultatFrance.nouvellesReanimations;
        } else {

          this.reanimations = this.resultatFranceModif.reanimation;
          this.nouvellesReanimations = this.resultatFranceModif.nouvellesReanimations;
        }
        this.progConta =
          ((this.resultatFrance.casConfirmes -
            this.resultatFranceVeille.casConfirmes) /
            this.resultatFranceVeille.casConfirmes) *
          100;

          if(Number.isNaN(this.diffGueris)){
            this.diffGueris = false;
          }
          if(Number.isNaN(this.diffConta)  || Number.isNaN(this.progConta) ){
            this.diffConta == false;
            this.progConta == false;
          }
          if(Number.isNaN(this.progHospi)){
            this.progHospi == false; 
          }
      });

    }

  }



  creationGraphique() {
    let tab = [];
    let tab2 = [];
    let tab3 = [];
    let tab4 = [];
    let mySet = new Set();
    let myMap = new Map();
    let myMap2 = new Map();
    this.serviceResultat.getCasConfirmesForFrance().subscribe((res: any) => {
      tab = res.allDataByDepartement;
      tab.forEach(element => {
        if (element.casConfirmes >= 0 && element.casConfirmes != undefined) {
          myMap.set(element.date, element.casConfirmes);
          mySet.add(element.date);
        }
        if (element.hospitalises >= 0 && element.hospitalises != undefined && element.source.nom == 'Ministère des Solidarités et de la Santé' && element.casConfirmes != undefined || element.hospitalises >= 0 && element.hospitalises != undefined && element.source.nom == 'Santé publique France' || element.hospitalises >= 0 && element.hospitalises == undefined && element.source.nom == 'OpenCOVID19-fr') {
          myMap2.set(element.date, element.hospitalises);
        } else if (element.hospitalises == undefined && element.casConfirmes >= 0 && element.casConfirmes != undefined) {
          myMap2.set(element.date, 0);
        }
      });
      for (let key of myMap.keys()) {
        tab3.push(key);
      }
      for (let value of myMap.values()) {
        tab2.push(value);
      }
      for (let value of myMap2.values()) {
        tab4.push(value);
      }
    })
    console.log(myMap2);
    const xAxisData = tab3;
    const data = tab2;
    const data2 = tab4;
    console.log(data);
    console.log(data2);

    this.options = {
      legend: {
        data: ['Contaminations', 'Hospitalisations'],
        align: 'left',
        textStyle: {
          color: '#d48265'
        }
      },
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: 'Contaminations',
          type: 'bar',
          data: data,
          animationDelay: (idx) => idx * 10,
        },
        {
          name: 'Hospitalisations',
          type: 'bar',
          data: data2,
          animationDelay: (idx) => idx * 10,
        }

      ],
      color: ['#61a0a8', '#bda29a'],
      textStyle: {
        color: '#ffffff'

      },
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };

  }

  changeEvent(event) {

    this.date = event.value;
    const format = 'yyyy-MM-dd';
    const myDate = this.date;
    const locale = 'en-US';
    let dateVeille;
    const formattedDate = formatDate(myDate, format, locale);
    let newDate = new Date(this.date);
    let dateCompare = new Date();
    console.log(dateCompare.getDate() + " " + (dateCompare.getMonth() + 1) + " " + dateCompare.getFullYear());
    if ((newDate.getDate() < 18 && (newDate.getMonth() + 1) <= 3 && newDate.getFullYear() <= 2020) || (newDate.getFullYear() <= 2019)) {
      this.router.navigate(['/error'])

    } else if (newDate > dateCompare) {
      this.router.navigate(['/error'])

    }

    console.log(newDate);
    if (newDate.getDate() < 10 && (newDate.getMonth() + 1) < 10) {
      if (newDate.getDate() == 1 && ((newDate.getMonth() + 1) % 2 == 0 && newDate.getFullYear() % 400 != 0 && (newDate.getMonth() + 1) <= 8) || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 9) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '31';
        console.log("1");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 5 || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 7) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '30';
        console.log("2");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 1) {
        dateVeille =
          (newDate.getFullYear() - 1) +
          '-' + '12' + '-' +
          '31';
        console.log("3");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 3) {
        if (newDate.getFullYear() % 400 != 0) {
          dateVeille =
            newDate.getFullYear() +
            '-' + '0' +
            newDate.getMonth() +
            '-' + '29';
          console.log("4");
        } else {
          dateVeille =
            newDate.getFullYear() +
            '-' + '0' +
            newDate.getMonth() +
            '-' + '28';
          console.log("5");
        }
      } else if (newDate.getDate() != 1) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          (newDate.getMonth() + 1) +
          '-' + '0' +
          (newDate.getDate() - 1);
        console.log("6");
      }
      console.log("7");
    } else if (newDate.getDate() > 10 && (newDate.getMonth() + 1) >= 10) {
      dateVeille =
        newDate.getFullYear() +
        '-' +
        (newDate.getMonth() + 1) +
        '-' +
        (newDate.getDate() - 1);
      console.log("8");
    } else if (newDate.getDate() == 10 && (newDate.getMonth() + 1) >= 10) {
      dateVeille =
        newDate.getFullYear() +
        '-' +
        (newDate.getMonth() + 1) +
        '-' + '0' +
        (newDate.getDate() - 1);
      console.log("88");
    } else if (newDate.getDate() < 10 && (newDate.getMonth() + 1) >= 10) {

      if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 11) {
        dateVeille =
          newDate.getFullYear() +
          '-' +
          newDate.getMonth() +
          '-' + '31';
        console.log("9");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) % 2 == 0 && (newDate.getMonth() + 1) != 12) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '30';
        console.log("10");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) % 2 == 0 && (newDate.getMonth() + 1) == 12) {
        dateVeille =
          newDate.getFullYear() +
          '-' +
          newDate.getMonth() +
          '-' + '30';
        console.log("11");
      } else if (newDate.getDate() != 1) {
        dateVeille =
          newDate.getFullYear() +
          '-' +
          (newDate.getMonth() + 1) +
          '-' + '0' +
          (newDate.getDate() - 1);
        console.log("15");
      }
    } else if (newDate.getDate() >= 10 && (newDate.getMonth() + 1) < 10) {

      if (newDate.getDate() == 1 && ((newDate.getMonth() + 1) % 2 == 0 && newDate.getFullYear() % 400 != 0 && (newDate.getMonth() + 1) <= 8) || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 9) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '31';
        console.log("16");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) % 3 == 0 || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 5 || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 7) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '30';
        console.log("17");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 1) {
        dateVeille =
          (newDate.getFullYear() - 1) +
          '-' + '12' + '-' +
          '31';
        console.log("18");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 3) {
        if (newDate.getFullYear() % 400 != 0) {
          dateVeille =
            newDate.getFullYear() +
            '-' + '0' +
            newDate.getMonth() +
            '-' + '28';
          console.log("19");
        } else {
          dateVeille =
            newDate.getFullYear() +
            '-' + '0' +
            newDate.getMonth() +
            '-' + '29';
          console.log("20");
        }
      } else if (newDate.getDate() != 10) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          (newDate.getMonth() + 1) +
          '-' +
          (newDate.getDate() - 1);
        console.log("21");
      }
      else if (newDate.getDate() == 10) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          (newDate.getMonth() + 1) +
          '-' + '0' +
          (newDate.getDate() - 1);
        console.log("22");
      }
      console.log("23");
    }

    this.serviceResultat.get2ResultatByDate(formattedDate, dateVeille).subscribe((res: any[]) => {
      console.log(dateVeille);
      console.log(formattedDate);
      this.resultat = res[0].allFranceDataByDate;
      this.resultatVeille = res[1].allFranceDataByDate;

      this.resultatRegion = this.resultat.filter(x => x.nom == "Île-de-France" || x.nom == "Auvergne-Rhône-Alpes" || x.nom == "Hauts-de-France" || x.nom == "Provence-Alpes-Côte d'Azur" || x.nom == "Occitanie" || x.nom == "Grand Est"
        || x.nom == "Nouvelle-Aquitaine" || x.nom == "Normandie" || x.nom == "Bourgogne-Franche-Comté" || x.nom == "Pays de la Loire" || x.nom == "Bretagne" || x.nom == "Corse" || x.nom == "Centre-Val de Loire");

      this.rows = this.resultatRegion;
      this.temp = [...this.resultatRegion];

      this.resultatFranceVeille = this.resultatVeille.find(
        x => x.code == 'FRA' && x.nom == 'France' && x.source.nom == 'Ministère des Solidarités et de la Santé' || x.code == 'FRA' && x.nom == 'France' && x.source.nom == 'OpenCOVID19-fr'
      );
      this.resultatFrance = this.resultat.find(
        x => x.code == 'FRA' && x.nom == 'France' && x.source.nom == 'Ministère des Solidarités et de la Santé' || x.code == 'FRA' && x.nom == 'France' && x.source.nom == 'OpenCOVID19-fr'
      );

      this.resultatFranceModif = this.resultat.find(x => x.code == 'FRA' && x.nom == 'France' && x.source.nom != 'Ministère des Solidarités et de la Santé');

      this.resultatFranceVeilleModif = this.resultatVeille.find(x => x.code == 'FRA' && x.nom == 'France' && x.source.nom != 'Ministère des Solidarités et de la Santé');

      if (this.resultatFrance.hospitalises != undefined && this.resultatFrance.nouvellesHospitalisations != undefined) {
        this.hospitalisation = this.resultatFrance.hospitalises;
        this.nouvellesHospitalisation = this.resultatFrance.nouvellesHospitalisations;
      } else if (this.resultatFrance.hospitalises != undefined && this.resultatFrance.nouvellesHospitalisations == undefined) {
        this.hospitalisation = this.resultatFrance.hospitalises;
        this.nouvellesHospitalisation = this.resultatFranceModif.nouvellesHospitalisations;
      } else if (this.resultatFrance.hospitalises == undefined && this.resultatFrance.nouvellesHospitalisations != undefined) {
        this.hospitalisation = this.resultatFranceModif.hospitalises;
        this.nouvellesHospitalisation = this.resultatFrance.nouvellesHospitalisations;
      } else {
        this.hospitalisation = this.resultatFranceModif.hospitalises;
        this.nouvellesHospitalisation = this.resultatFranceModif.nouvellesHospitalisations;
      }

      // if (this.resultatFranceVeille.hospitalises != undefined && this.resultatFranceVeille.nouvellesHospitalisations != undefined) {
      //   this.hospitalisation = this.resultatFranceVeille.hospitalises;
      //   this.nouvellesHospitalisation = this.resultatFranceVeille.nouvellesHospitalisations;
      // } else if (this.resultatFranceVeille.hospitalises != undefined && this.resultatFranceVeille.nouvellesHospitalisations == undefined) {
      //   this.hospitalisation = this.resultatFrance.hospitalises;
      //   this.nouvellesHospitalisation = this.resultatFranceVeilleModif.nouvellesHospitalisations;
      // } else if (this.resultatFranceVeille.hospitalises == undefined && this.resultatFranceVeille.nouvellesHospitalisations != undefined) {
      //   this.hospitalisation = this.resultatFranceVeilleModif.hospitalises;
      //   this.nouvellesHospitalisation = this.resultatFranceVeille.nouvellesHospitalisations;
      // } else {
      //   this.hospitalisation = undefined;
      //   this.nouvellesHospitalisation = undefined;
      // }


      if (this.resultatFrance.deces != undefined) {
        if (this.resultatFrance.decesEhpad != undefined) {
          this.decesAvecEhpad = this.resultatFrance.deces + this.resultatFrance.decesEhpad;
          console.log(this.decesAvecEhpad);
          this.deces = undefined;
        } else {
          this.deces = this.resultatFrance.deces;
          this.decesAvecEhpad = undefined;
          console.log(this.deces);
        }
      } else {
        if (this.resultatFranceModif.decesEhpad != undefined) {
          this.decesAvecEhpad = this.resultatFranceModif.deces + this.resultatFranceModif.decesEhpad;
          console.log(this.decesAvecEhpad);
          this.deces = undefined;
        } else {
          this.deces = this.resultatFranceModif.deces;
          this.decesAvecEhpad = undefined;
          console.log(this.deces);
        }
      }


      if (this.resultatFranceVeille.deces != undefined) {
        if (this.resultatFranceVeille.decesEhpad != undefined) {
          this.decesVeilleAvecEhpad = this.resultatFranceVeille.deces + this.resultatFranceVeille.decesEhpad;
          this.decesVeille = undefined;
        } else {
          this.decesVeille = this.resultatFranceVeille.deces;
          this.decesVeilleAvecEhpad = undefined;
        }
      } else {
        if (this.resultatFranceVeilleModif.decesEhpad != undefined) {
          this.decesVeilleAvecEhpad = this.resultatFranceVeilleModif.deces + this.resultatFranceVeilleModif.decesEhpad;
          this.decesVeille = undefined;
        } else {
          this.decesVeille = this.resultatFranceVeilleModif.deces;
          this.decesVeilleAvecEhpad = undefined;
        }
      }

      if (this.decesAvecEhpad == undefined || this.decesVeilleAvecEhpad == undefined) {
        this.diffDeces = this.deces - this.decesVeille;

        this.progDeces = ((this.deces - this.decesVeille) / this.decesVeille) * 100;
      } else if (this.decesAvecEhpad != undefined && this.decesVeilleAvecEhpad != undefined) {
        this.diffDeces = this.decesAvecEhpad - this.decesVeilleAvecEhpad;

        this.progDeces = ((this.decesAvecEhpad - this.decesVeilleAvecEhpad) / this.decesVeilleAvecEhpad) * 100;
      }

      if (Number.isNaN(this.diffDeces)) {
        this.diffDeces = false;
      }
      if (Number.isNaN(this.progDeces)) {
        this.progDeces = false;
      }

      if (this.hospitalisation != undefined && this.nouvellesHospitalisation != undefined) {
        this.progHospi =
          (this.nouvellesHospitalisation /
            (this.hospitalisation -
              this.nouvellesHospitalisation)) *
          100;
      } else { this.progHospi = undefined; }

      this.diffConta = this.resultatFrance.casConfirmes - this.resultatFranceVeille.casConfirmes;

      if (this.resultatFrance.gueris != undefined) {
        this.guerrison = this.resultatFrance.gueris;
        this.diffGueris = this.resultatFrance.gueris - this.resultatFranceVeille.gueris;
      } else if (this.resultatFrance.gueris == undefined) {
        this.guerrison = this.resultatFranceModif.gueris;
        this.diffGueris = this.resultatFranceModif.gueris - this.resultatFranceVeilleModif.gueris;
      }

      if (this.resultatFrance.reanimation != undefined) {
        console.log(this.resultatFrance.reanimation);
        this.reanimations = this.resultatFrance.reanimation;
        this.nouvellesReanimations = this.resultatFrance.nouvellesReanimations;
      } else if (this.resultatFrance.reanimation == undefined && this.resultatFranceModif.reanimation != undefined) {
        console.log(this.resultatFrance.reanimation);
        this.reanimations = this.resultatFranceModif.reanimation;
        this.nouvellesReanimations = this.resultatFranceModif.nouvellesReanimations;
      }
      else if (this.resultatFrance.reanimation == undefined && this.resultatFranceVeille.reanimation == undefined && this.resultatFranceModif.reanimation == undefined && this.resultatFranceVeilleModif.reanimation == undefined) {
        this.reanimations = undefined;
        this.nouvellesReanimations = undefined;
      }
      this.progConta =
        ((this.resultatFrance.casConfirmes -
          this.resultatFranceVeille.casConfirmes) /
          this.resultatFranceVeille.casConfirmes) *
        100;
        if(Number.isNaN(this.diffGueris)){
          this.diffGueris = false;
        }
        if(Number.isNaN(this.diffConta)  || Number.isNaN(this.progConta) ){
          this.diffConta == false;
          this.progConta == false;
        }
        if(Number.isNaN(this.progHospi)){
          this.progHospi == false; 
        }

    });
  }
  transmettreRegion(region) {
    var nRegion;
    console.log(region);
    switch (region) {
      case "Île-de-France":
        nRegion = 1;
        break;
      case "Auvergne-Rhône-Alpes":
        nRegion = 2;
        break;
      case "Hauts-de-France":
        nRegion = 3;
        break;
      case "Provence-Alpes-Côte d'Azur":
        nRegion = 4;
        break;
      case "Occitanie":
        nRegion = 5;
        break;
      case "Nouvelle-Aquitaine":
        nRegion = 6;
        break;
      case "Grand Est":
        nRegion = 7;
        break;
      case "Normandie":
        nRegion = 8;
        break;
      case "Bourgogne-Franche-Comté":
        nRegion = 9;
        break;
      case "Pays de la Loire":
        nRegion = 10;
        break;
      case "Bretagne":
        nRegion = 11;
        break;
      case "Corse":
        nRegion = 12;
        break;
      case "Centre-Val de Loire":
        nRegion = 13;
        break;
    }
    this.router.navigate(['/region/' + nRegion]);

  }
}