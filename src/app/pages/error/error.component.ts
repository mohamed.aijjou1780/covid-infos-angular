import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { Router } from '@angular/router';
@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  constructor(private _location: Location, private router:Router) { }

  ngOnInit(): void {



  }
  navigate(){
    this.router.navigate(['/jour'])
  }

}
