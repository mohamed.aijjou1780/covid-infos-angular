import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AfficheServiceService } from 'src/app/services/affiche-service.service';
import { Resultat } from 'src/app/models/resultat';
import { formatDate, DatePipe } from '@angular/common';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { faVirus } from '@fortawesome/free-solid-svg-icons';
import { faHospital } from '@fortawesome/free-solid-svg-icons';
import { faMale } from '@fortawesome/free-solid-svg-icons';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { faPercentage } from '@fortawesome/free-solid-svg-icons';
import { faProcedures } from '@fortawesome/free-solid-svg-icons';
import { faVirusSlash } from '@fortawesome/free-solid-svg-icons';
import { faChartBar } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss'],
  providers: [DatePipe]
})
export class RegionComponent implements OnInit {
  nomRegion: any;
  date: string;
region : any;
  resultatRegionVeille: Resultat[];
  resultatDep: Resultat[];
  resultatRegion: Resultat[];

  config: any;
  deces: number;
  decesVeille: number;
  rows = [];
  temp = [];
  resultatRegionR: any;
  resultatRegionRVeille: any;
  resultatRegionModif: any;
  resultatRegionVeilleModif: any;
  faCoffee = faCoffee;
  faVirus = faVirus;
  faHospital = faHospital;
  faMale = faMale;
  faUserPlus = faUserPlus;
  faPercentage = faPercentage;
  faProcedures = faProcedures;
  faVirusSlash = faVirusSlash;
  faChartBar = faChartBar;
  faInfoCircle = faInfoCircle;
  progHospi: any;
  progConta: any;
  diffConta: any;
  infosDeces: boolean;
  infosDecesVeille: boolean;
  diffDeces: any;
  progDeces: any;
  decesAvecEhpad = undefined;
  decesVeilleAvecEhpad: number;
  showSpinner = true;
  diffGueris: any;
  options: any;
  panelOpenState = false;
  guerrison: number;
  reanimations: number;
  nouvellesReanimations: number;
  hospitalisation: number;
  nouvellesHospitalisation: number;
  initOpts = {
    renderer: 'svg',
    width: 600,
    height: 400,
  };
  tt = [];
  @ViewChild(DatatableComponent) table: DatatableComponent;


  constructor(private route: ActivatedRoute, private serviceResultat: AfficheServiceService,
    private datePipe: DatePipe, private router: Router) {
      this.nomRegion = this.route.snapshot.paramMap.get('code');  
    switch (this.nomRegion) {
      case "1":
        this.region = "Île-de-France";
        break;
      case "2":
        this.region = "Auvergne";
        break;
      case "3":
        this.region = "Hauts-de-France";
        break;
      case "4":
        this.region = "Provence-Alpes-Côte d'Azur";
        break;
      case "5":
        this.region = "Occitanie";
        break;
      case "6":
        this.region = "Nouvelle-Aquitaine";
        break;
      case "7":
        this.region = "Grand Est";
        break;
      case "8":
        this.region = "Normandie";
        break;
      case "9":
        this.region = "Bourgogne-Franche-Comté";
        break;
      case "10":
        this.region = "Pays de la Loire";
        break;
      case "11":
        this.region = "Bretagne";
        break;
      case "12":
        this.region = "Corse";
        break;
      case "13":
        this.region = "Centre-Val de Loire";
        break;
    }
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nom.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  ngOnInit(): void {

    this.creationGraphique();
    const datec = new Date();
    console.log(datec);
    let dateVeille;
    let infos;
    let dateAvantVeille;
    const formattedDate = this.datePipe.transform(datec, 'yyyy-MM-dd');
    console.log(formattedDate);
    console.log(datec.getHours());
    if (datec.getHours() > 17) {
      infos = true;
    } else {
      infos = false;
    }
    console.log(infos);

    if (datec.getHours() < 21) {
      if (datec.getDate() < 10 && (datec.getMonth() + 1) < 10) {
        if (datec.getDate() == 1 && (datec.getMonth() + 1) <= 8 || datec.getDate() == 1 && (datec.getMonth() + 1) == 9) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          console.log('1');
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 5 || datec.getDate() == 1 && (datec.getMonth() + 1) == 7) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log('2');

        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 1) {
          dateVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          dateAvantVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          console.log('3');
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 3) {
          if (datec.getFullYear() % 400 != 0) {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log("bisext");
            dateAvantVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log('4');
          } else {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            dateAvantVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            console.log("pas bisext");
          }
        } else if (datec.getDate() != 1) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 2);
        }
      } else if (datec.getDate() > 10 && (datec.getMonth() + 1) >= 10) {
        dateVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' +
          (datec.getDate() - 1);
        dateAvantVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' +
          (datec.getDate() - 2);
      } else if (datec.getDate() == 10 && (datec.getMonth() + 1) >= 10) {
        dateVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' + '0' +
          (datec.getDate() - 1);
        dateAvantVeille =
          datec.getFullYear() +
          '-' + '0' +
          (datec.getMonth() + 1) +
          '-' +
          (datec.getDate() - 2);
      } else if (datec.getDate() < 10 && (datec.getMonth() + 1) >= 10) {
        if (datec.getDate() == 1 && (datec.getMonth() + 1) == 11) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '31';
          dateAvantVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '30';
          console.log("9");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) != 12) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("10");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '29';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) == 12) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '30';
          console.log("11");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '29';
        } else if (datec.getDate() != 1 && datec.getDate() != 2) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          dateAvantVeille =
            datec.getFullYear() +
            '-' + 
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 2);
        }else if (datec.getDate() == 2 && (datec.getMonth() + 1) == 11) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          dateAvantVeille =
            datec.getFullYear() +
            '-' + 
            (datec.getMonth() ) +
            '-' + '31';
            console.log('coucou');
        }else if (datec.getDate() == 2 && (datec.getMonth() + 1) == 12) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          dateAvantVeille =
            datec.getFullYear() +
            '-' + 
            (datec.getMonth() ) +
            '-' + '30';
        }
      } else if (datec.getDate() >= 10 && (datec.getMonth() + 1) < 10) {
        if (datec.getDate() == 1 && ((datec.getMonth() + 1) % 2 == 0 && datec.getFullYear() % 400 != 0 && (datec.getMonth() + 1) <= 8) || datec.getDate() == 1 && (datec.getMonth() + 1) == 9) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          console.log("16");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 3 == 0 || datec.getDate() == 1 && (datec.getMonth() + 1) == 5 || datec.getDate() == 1 && (datec.getMonth() + 1) == 7) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("17");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '29';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 1) {
          dateVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          console.log("18");
          dateAvantVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '30';
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 3) {
          if (datec.getFullYear() % 400 != 0) {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            console.log("19");
            dateAvantVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '27';
          } else {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log("20");
            dateAvantVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
          }
        } else if (datec.getDate() != 10) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' +
            (datec.getDate() - 1);

          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' +
            (datec.getDate() - 2);
        }
        else if (datec.getDate() == 10) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          console.log("22");
          dateAvantVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 2);
        }

      }
    
      console.log(dateVeille);
      console.log(dateAvantVeille);
      this.serviceResultat.get2ResultatByDate(dateVeille, dateAvantVeille).subscribe((res: any[]) => {
       
        this.resultatRegion = res[0].allFranceDataByDate;
        this.resultatRegionVeille = res[1].allFranceDataByDate;

        if(this.nomRegion == 1){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Paris" || x.nom == "Hauts-de-Seine" || x.nom == "Seine-Saint-Denis" || x.nom == "Val-de-Marne" || x.nom == "Val-d'Oise" || x.nom == "Essonne"
          || x.nom == "Yvelines" || x.nom == "Seine-et-Marne");
     
        }
        else if(this.nomRegion== 2){
          
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Auvergne" || x.nom == "Ain" || x.nom == "Allier" || x.nom == "Ardèche" || x.nom == "Cantal" || x.nom == "Drôme"
          || x.nom == "Isère" || x.nom == "Loire" ||  x.nom == "Haute-Loire" || x.nom == "Puy-de-Dôme" || x.nom == "Rhône" || x.nom == "Savoie" || x.nom == "Haute-Savoie");
        
        }
       else if(this.nomRegion== 3){
     
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Aisne" || x.nom == "Nord" || x.nom == "Oise" || x.nom == "Pas-de-Calais" || x.nom == "Somme");

         
        }
       else if(this.nomRegion == 4){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Alpes-de-Haute-Provence" || x.nom == "Hautes-Alpes" || x.nom == "Alpes-Maritimes" || x.nom == "Bouches-du-Rhône" || x.nom == "Var" || x.nom == "Vaucluse");
          
        }
       else if(this.nomRegion == 5){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Ariège" || x.nom == "Aude" || x.nom == "Aveyron" || x.nom == "Gard" || x.nom == "Haute-Garonne" || x.nom == "Gers" );
         
        }
     else if(this.nomRegion == 6){
       
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Charente" || x.nom == "Charente-Maritime" || x.nom == "Corrèze" || x.nom == "Creuse" || x.nom == "Dordogne" || x.nom == "Gironde" 
          || x.nom == "Landes" || x.nom == "Lot-et-Garonne" || x.nom == "Pyrénées-Atlantiques" || x.nom == "Deux-Sèvres" || x.nom == "Vienne" || x.nom == "Haute-Vienne");
        
        }
       else if(this.nomRegion == 7){
       
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Ardennes" || x.nom == "Aube" || x.nom == "Marne" || x.nom == "Haute-Marne" || x.nom == "Meurthe-et-Moselle" || x.nom == "Meuse" 
          || x.nom == "Moselle" || x.nom == "Bas-Rhin" || x.nom == "Haut-Rhin" || x.nom == "Haut-Rhin" || x.nom == "Vienne" || x.nom == "Vosges");
          
        }
       else if(this.nomRegion == 8){
        
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Calvados" || x.nom == "Eure" || x.nom == "Manche" || x.nom == "Orne" || x.nom == "Seine-Maritime");
        
        }
      else if(this.nomRegion == 9){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Côte-d'Or" || x.nom == "Doubs" || x.nom == "Jura" || x.nom == "Nièvre" || x.nom == "Haute-Saône" || x.nom == "Saône-et-Loire" 
          || x.nom == "Yonne" || x.nom == "Territoire de Belfort");
    
        }
      else if(this.nomRegion == 10){
      
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Loire-Atlantique" || x.nom == "Maine-et-Loire" || x.nom == "Mayenne" || x.nom == "Sarthe" || x.nom == "Vendée" );
       
        }
       else if(this.nomRegion == 11){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Côtes-d'Armor" || x.nom == "Finistère" || x.nom == "Ille-et-Vilaine" || x.nom == "Morbihan");
      
        }
       else if(this.nomRegion == 12){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Haute-Corse" || x.nom == "Corse-du-Sud");
         
        }
       else if(this.nomRegion == 13){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Cher" || x.nom == "Eure-et-Loir" || x.nom == "Indre" || x.nom == "Indre-et-Loire" || x.nom == "Loir-et-Cher" || x.nom == "Loiret");
       
        }
      
        this.rows = this.resultatDep;
        this.temp = [...this.resultatDep];
        this.resultatRegionRVeille = this.resultatRegionVeille.find(
          x => x.nom == this.region
        );
        this.resultatRegionR = this.resultatRegion.find(
          x => x.nom == this.region
        );

        this.resultatRegionModif = this.resultatRegion.find(x => x.nom == this.region && x.source.nom == 'OpenCOVID19-fr');
     
        this.resultatRegionVeilleModif = this.resultatRegionVeille.find(x => x.nom == this.region && x.source.nom == 'OpenCOVID19-fr');
     
     
        if (this.resultatRegionR.hospitalises != undefined && this.resultatRegionR.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatRegionR.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionR.nouvellesHospitalisations;
        } else if (this.resultatRegionR.hospitalises != undefined && this.resultatRegionR.nouvellesHospitalisations == undefined) {
          this.hospitalisation = this.resultatRegionR.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionModif.nouvellesHospitalisations;
        } else if (this.resultatRegionR.hospitalises == undefined && this.resultatRegionR.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatRegionModif.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionR.nouvellesHospitalisations;
        } else {
          this.hospitalisation = undefined;
          this.nouvellesHospitalisation = undefined;
        }

        if (this.resultatRegionRVeille.hospitalises != undefined && this.resultatRegionRVeille.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatRegionRVeille.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionRVeille.nouvellesHospitalisations;
        } else if (this.resultatRegionRVeille.hospitalises != undefined && this.resultatRegionRVeille.nouvellesHospitalisations == undefined) {
          this.hospitalisation = this.resultatRegionR.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionVeilleModif.nouvellesHospitalisations;
        } else if (this.resultatRegionRVeille.hospitalises == undefined && this.resultatRegionRVeille.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatRegionVeilleModif.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionRVeille.nouvellesHospitalisations;
        } else {
          this.hospitalisation = undefined;
          this.nouvellesHospitalisation = undefined;
        }


        if (this.resultatRegionR.deces != undefined) {
          if (this.resultatRegionR.decesEhpad != undefined) {
            this.decesAvecEhpad = this.resultatRegionR.deces + this.resultatRegionR.decesEhpad;
            console.log(this.decesAvecEhpad);

          } else {
            this.deces = this.resultatRegionR.deces;

          }
        } else {
          if (this.resultatRegionModif.decesEhpad != undefined) {
            this.decesAvecEhpad = this.resultatRegionModif.deces + this.resultatRegionModif.decesEhpad;
            console.log(this.decesAvecEhpad);

          } else {
            this.deces = this.resultatRegionModif.deces;

          }
        }


        if (this.resultatRegionRVeille.deces != undefined) {
          if (this.resultatRegionRVeille.decesEhpad != undefined) {
            this.decesVeilleAvecEhpad = this.resultatRegionRVeille.deces + this.resultatRegionRVeille.decesEhpad;

          } else {
            this.decesVeille = this.resultatRegionRVeille.deces;

          }
        } else {
          if (this.resultatRegionVeilleModif.decesEhpad != undefined) {
            this.decesVeilleAvecEhpad = this.resultatRegionVeilleModif.deces + this.resultatRegionVeilleModif.decesEhpad;

          } else {
            this.decesVeille = this.resultatRegionVeilleModif.deces;

          }
        }

        if (this.decesAvecEhpad == undefined || this.decesVeilleAvecEhpad == undefined) {
          this.diffDeces = this.deces - this.decesVeille;

          this.progDeces = ((this.deces - this.decesVeille) / this.decesVeille) * 100;
        } else if (this.decesAvecEhpad != undefined && this.decesVeilleAvecEhpad != undefined) {
          this.diffDeces = this.decesAvecEhpad - this.decesVeilleAvecEhpad;

          this.progDeces = ((this.decesAvecEhpad - this.decesVeilleAvecEhpad) / this.decesVeilleAvecEhpad) * 100;
        }

        if(Number.isNaN(this.diffDeces)){
          this.diffDeces = false;
        }
        if(Number.isNaN(this.progDeces)){
          this.progDeces = false;
        }

        if (this.hospitalisation != undefined && this.nouvellesHospitalisation != undefined) {
          this.progHospi =
            (this.nouvellesHospitalisation /
              (this.hospitalisation -
                this.nouvellesHospitalisation)) *
            100;
        } else { this.progHospi = undefined; }

        this.diffConta = this.resultatRegionR.casConfirmes - this.resultatRegionRVeille.casConfirmes;

        if (this.resultatRegionR.gueris != undefined && this.resultatRegionRVeille != undefined) {
          this.guerrison = this.resultatRegionR.gueris;
          this.diffGueris = this.resultatRegionR.gueris - this.resultatRegionRVeille.gueris;
        } else {
          this.guerrison = this.resultatRegionModif.gueris;
          this.diffGueris = this.resultatRegionModif.gueris - this.resultatRegionVeilleModif.gueris;
        }

        if (this.resultatRegionR.reanimation != undefined) {
          this.reanimations = this.resultatRegionR.reanimation;
          this.nouvellesReanimations = this.resultatRegionR.nouvellesReanimations;
        } else {

          this.reanimations = this.resultatRegionModif.reanimation;
          this.nouvellesReanimations = this.resultatRegionModif.nouvellesReanimations;
        }
        this.progConta =
          ((this.resultatRegionR.casConfirmes -
            this.resultatRegionRVeille.casConfirmes) /
            this.resultatRegionRVeille.casConfirmes) *
          100;

          if(Number.isNaN(this.diffGueris)){
            this.diffGueris = false;
          }
          if(Number.isNaN(this.diffConta)  || Number.isNaN(this.progConta) ){
            this.diffConta == false;
            this.progConta == false;
          }
          if(Number.isNaN(this.progHospi)){
            this.progHospi == false; 
          }


      });

    
  

    } else {
      if (datec.getDate() < 10 && (datec.getMonth() + 1) < 10) {
        if (datec.getDate() == 1 && ((datec.getMonth() + 1) % 2 == 0 && datec.getFullYear() % 400 != 0 && (datec.getMonth() + 1) <= 8) || datec.getDate() == 1 && (datec.getMonth() + 1) == 9) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          console.log("1");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 5 || datec.getDate() == 1 && (datec.getMonth() + 1) == 7) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("2");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 1) {
          dateVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          console.log("3");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 3) {
          if (datec.getFullYear() % 400 != 0) {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log("4");
          } else {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            console.log("5");
          }
        } else if (datec.getDate() != 1) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          console.log("6");
        }
        console.log("7");
      } else if (datec.getDate() > 10 && (datec.getMonth() + 1) >= 10) {
        dateVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' +
          (datec.getDate() - 1);
        console.log("8");
      } else if (datec.getDate() == 10 && (datec.getMonth() + 1) >= 10) {
        dateVeille =
          datec.getFullYear() +
          '-' +
          (datec.getMonth() + 1) +
          '-' + '0' +
          (datec.getDate() - 1);
        console.log("88");
      } else if (datec.getDate() < 10 && (datec.getMonth() + 1) >= 10) {
        if (datec.getDate() == 1 && (datec.getMonth() + 1) == 11) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '31';
          console.log("9");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) != 12) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("10");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) == 12) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            datec.getMonth() +
            '-' + '30';
          console.log("11");
        } else if (datec.getDate() != 1) {
          dateVeille =
            datec.getFullYear() +
            '-' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          console.log("15");
        }
      } else if (datec.getDate() >= 10 && (datec.getMonth() + 1) < 10) {

        if (datec.getDate() == 1 && ((datec.getMonth() + 1) % 2 == 0 && (datec.getMonth() + 1) <= 8) || datec.getDate() == 1 && (datec.getMonth() + 1) == 9) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '31';
          console.log("16");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) % 3 == 0 || datec.getDate() == 1 && (datec.getMonth() + 1) == 5 || datec.getDate() == 1 && (datec.getMonth() + 1) == 7) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            datec.getMonth() +
            '-' + '30';
          console.log("17");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 1) {
          dateVeille =
            (datec.getFullYear() - 1) +
            '-' + '12' + '-' +
            '31';
          console.log("18");
        } else if (datec.getDate() == 1 && (datec.getMonth() + 1) == 3) {
          if (datec.getFullYear() % 400 != 0) {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '28';
            console.log("19");
          } else {
            dateVeille =
              datec.getFullYear() +
              '-' + '0' +
              datec.getMonth() +
              '-' + '29';
            console.log("20");
          }
        } else if (datec.getDate() != 10) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' +
            (datec.getDate() - 1);
          console.log("21");
        }
        else if (datec.getDate() == 10) {
          dateVeille =
            datec.getFullYear() +
            '-' + '0' +
            (datec.getMonth() + 1) +
            '-' + '0' +
            (datec.getDate() - 1);
          console.log("22");
        }
        console.log("23");
      }

      this.serviceResultat.get2ResultatByDate(formattedDate, dateVeille).subscribe((res: any[]) => {
        console.log(formattedDate);
        console.log(dateVeille);
        this.resultatRegion = res[0].allFranceDataByDate;
        this.resultatRegionVeille = res[1].allFranceDataByDate;

        if(this.nomRegion == 1){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Paris" || x.nom == "Hauts-de-Seine" || x.nom == "Seine-Saint-Denis" || x.nom == "Val-de-Marne" || x.nom == "Val-d'Oise" || x.nom == "Essonne"
          || x.nom == "Yvelines" || x.nom == "Seine-et-Marne");
     
        }
        else if(this.nomRegion== 2){
          
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Auvergne" || x.nom == "Ain" || x.nom == "Allier" || x.nom == "Ardèche" || x.nom == "Cantal" || x.nom == "Drôme"
          || x.nom == "Isère" || x.nom == "Loire" ||  x.nom == "Haute-Loire" || x.nom == "Puy-de-Dôme" || x.nom == "Rhône" || x.nom == "Savoie" || x.nom == "Haute-Savoie");
        
        }
       else if(this.nomRegion== 3){
     
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Aisne" || x.nom == "Nord" || x.nom == "Oise" || x.nom == "Pas-de-Calais" || x.nom == "Somme");

         
        }
       else if(this.nomRegion == 4){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Alpes-de-Haute-Provence" || x.nom == "Hautes-Alpes" || x.nom == "Alpes-Maritimes" || x.nom == "Bouches-du-Rhône" || x.nom == "Var" || x.nom == "Vaucluse");
          
        }
       else if(this.nomRegion == 5){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Ariège" || x.nom == "Aude" || x.nom == "Aveyron" || x.nom == "Gard" || x.nom == "Haute-Garonne" || x.nom == "Gers" );
         
        }
     else if(this.nomRegion == 6){
       
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Charente" || x.nom == "Charente-Maritime" || x.nom == "Corrèze" || x.nom == "Creuse" || x.nom == "Dordogne" || x.nom == "Gironde" 
          || x.nom == "Landes" || x.nom == "Lot-et-Garonne" || x.nom == "Pyrénées-Atlantiques" || x.nom == "Deux-Sèvres" || x.nom == "Vienne" || x.nom == "Haute-Vienne");
        
        }
       else if(this.nomRegion == 7){
       
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Ardennes" || x.nom == "Aube" || x.nom == "Marne" || x.nom == "Haute-Marne" || x.nom == "Meurthe-et-Moselle" || x.nom == "Meuse" 
          || x.nom == "Moselle" || x.nom == "Bas-Rhin" || x.nom == "Haut-Rhin" || x.nom == "Haut-Rhin" || x.nom == "Vienne" || x.nom == "Vosges");
          
        }
       else if(this.nomRegion == 8){
        
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Calvados" || x.nom == "Eure" || x.nom == "Manche" || x.nom == "Orne" || x.nom == "Seine-Maritime");
        
        }
      else if(this.nomRegion == 9){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Côte-d'Or" || x.nom == "Doubs" || x.nom == "Jura" || x.nom == "Nièvre" || x.nom == "Haute-Saône" || x.nom == "Saône-et-Loire" 
          || x.nom == "Yonne" || x.nom == "Territoire de Belfort");
    
        }
      else if(this.nomRegion == 10){
      
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Loire-Atlantique" || x.nom == "Maine-et-Loire" || x.nom == "Mayenne" || x.nom == "Sarthe" || x.nom == "Vendée" );
       
        }
       else if(this.nomRegion == 11){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Côtes-d'Armor" || x.nom == "Finistère" || x.nom == "Ille-et-Vilaine" || x.nom == "Morbihan");
      
        }
       else if(this.nomRegion == 12){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Haute-Corse" || x.nom == "Corse-du-Sud");
         
        }
       else if(this.nomRegion == 13){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Cher" || x.nom == "Eure-et-Loir" || x.nom == "Indre" || x.nom == "Indre-et-Loire" || x.nom == "Loir-et-Cher" || x.nom == "Loiret");
       
        }

        this.rows = this.resultatDep;
        this.temp = [...this.resultatDep];
        this.resultatRegionRVeille = this.resultatRegionVeille.find(
          x => x.nom == this.region
        );
        this.resultatRegionR = this.resultatRegion.find(
          x => x.nom == this.region
        );

        this.resultatRegionModif = this.resultatRegion.find(x => x.nom == this.region && x.source.nom == 'OpenCOVID19-fr');

        this.resultatRegionVeilleModif = this.resultatRegionVeille.find(x => x.nom == this.region && x.source.nom == 'OpenCOVID19-fr');

        if (this.resultatRegionR.hospitalises != undefined && this.resultatRegionR.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatRegionR.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionR.nouvellesHospitalisations;
        } else if (this.resultatRegionR.hospitalises != undefined && this.resultatRegionR.nouvellesHospitalisations == undefined) {
          this.hospitalisation = this.resultatRegionR.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionModif.nouvellesHospitalisations;
        } else if (this.resultatRegionR.hospitalises == undefined && this.resultatRegionR.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatRegionModif.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionR.nouvellesHospitalisations;
        } else {
          this.hospitalisation = undefined;
          this.nouvellesHospitalisation = undefined;
        }

        if (this.resultatRegionRVeille.hospitalises != undefined && this.resultatRegionRVeille.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatRegionRVeille.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionRVeille.nouvellesHospitalisations;
        } else if (this.resultatRegionRVeille.hospitalises != undefined && this.resultatRegionRVeille.nouvellesHospitalisations == undefined) {
          this.hospitalisation = this.resultatRegionR.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionVeilleModif.nouvellesHospitalisations;
        } else if (this.resultatRegionRVeille.hospitalises == undefined && this.resultatRegionRVeille.nouvellesHospitalisations != undefined) {
          this.hospitalisation = this.resultatRegionVeilleModif.hospitalises;
          this.nouvellesHospitalisation = this.resultatRegionRVeille.nouvellesHospitalisations;
        } else {
          this.hospitalisation = undefined;
          this.nouvellesHospitalisation = undefined;
        }


        if (this.resultatRegionR.deces != undefined) {
          if (this.resultatRegionR.decesEhpad != undefined) {
            this.decesAvecEhpad = this.resultatRegionR.deces + this.resultatRegionR.decesEhpad;
            console.log(this.decesAvecEhpad);

          } else {
            this.deces = this.resultatRegionR.deces;

          }
        } else {
          if (this.resultatRegionModif.decesEhpad != undefined) {
            this.decesAvecEhpad = this.resultatRegionModif.deces + this.resultatRegionModif.decesEhpad;
            console.log(this.decesAvecEhpad);

          } else {
            this.deces = this.resultatRegionModif.deces;

          }
        }


        if (this.resultatRegionRVeille.deces != undefined) {
          if (this.resultatRegionRVeille.decesEhpad != undefined) {
            this.decesVeilleAvecEhpad = this.resultatRegionRVeille.deces + this.resultatRegionRVeille.decesEhpad;

          } else {
            this.decesVeille = this.resultatRegionRVeille.deces;

          }
        } else {
          if (this.resultatRegionVeilleModif.decesEhpad != undefined) {
            this.decesVeilleAvecEhpad = this.resultatRegionVeilleModif.deces + this.resultatRegionVeilleModif.decesEhpad;

          } else {
            this.decesVeille = this.resultatRegionVeilleModif.deces;

          }
        }

        if (this.decesAvecEhpad == undefined || this.decesVeilleAvecEhpad == undefined) {
          this.diffDeces = this.deces - this.decesVeille;

          this.progDeces = ((this.deces - this.decesVeille) / this.decesVeille) * 100;
        } else if (this.decesAvecEhpad != undefined && this.decesVeilleAvecEhpad != undefined) {
          this.diffDeces = this.decesAvecEhpad - this.decesVeilleAvecEhpad;

          this.progDeces = ((this.decesAvecEhpad - this.decesVeilleAvecEhpad) / this.decesVeilleAvecEhpad) * 100;
        }

        if(Number.isNaN(this.diffDeces)){
          this.diffDeces = false;
        }
        if(Number.isNaN(this.progDeces)){
          this.progDeces = false;
        }

        if (this.hospitalisation != undefined && this.nouvellesHospitalisation != undefined) {
          this.progHospi =
            (this.nouvellesHospitalisation /
              (this.hospitalisation -
                this.nouvellesHospitalisation)) *
            100;
        } else { this.progHospi = undefined; }

        this.diffConta = this.resultatRegionR.casConfirmes - this.resultatRegionRVeille.casConfirmes;

        if (this.resultatRegionR.gueris != undefined && this.resultatRegionRVeille != undefined) {
          this.guerrison = this.resultatRegionR.gueris;
          this.diffGueris = this.resultatRegionR.gueris - this.resultatRegionRVeille.gueris;
        } else {
          this.guerrison = this.resultatRegionModif.gueris;
          this.diffGueris = this.resultatRegionModif.gueris - this.resultatRegionVeilleModif.gueris;
        }

        if (this.resultatRegionR.reanimation != undefined) {
          this.reanimations = this.resultatRegionR.reanimation;
          this.nouvellesReanimations = this.resultatRegionR.nouvellesReanimations;
        } else {

          this.reanimations = this.resultatRegionModif.reanimation;
          this.nouvellesReanimations = this.resultatRegionModif.nouvellesReanimations;
        }
        this.progConta =
          ((this.resultatRegionR.casConfirmes -
            this.resultatRegionRVeille.casConfirmes) /
            this.resultatRegionRVeille.casConfirmes) *
          100;


          if(Number.isNaN(this.diffGueris)){
            this.diffGueris = false;
          }
          if(Number.isNaN(this.diffConta)  || Number.isNaN(this.progConta) ){
            this.diffConta == false;
            this.progConta == false;
          }
          if(Number.isNaN(this.progHospi)){
            this.progHospi == false; 
          }

      });
    }
  }



  creationGraphique() {

    let tab = [];
    let tab2 = [];
    let tab3 = [];
    let tab4 = [];
    let tab5 = [];
    let mySet = new Set();
    let myMap = new Map();
    let myMap2 = new Map();
    let myMap3 = new Map();
    console.log(this.region);
    this.serviceResultat.getResultatRegion(this.region).subscribe((res: any) => {
      tab = res.allDataByDepartement;
     
      tab.forEach(element => {
        if (element.hospitalises >= 0 && element.hospitalises != undefined) {
          myMap.set(element.date, element.hospitalises);
          mySet.add(element.date);
        }
        if (element.deces >= 0 && element.deces != undefined ) {
          myMap2.set(element.date, element.deces);
        } else if (element.deces == undefined && element.hospitalises >= 0 && element.hospitalises != undefined) {
          myMap2.set(element.date, 0);
        }
        if (element.gueris >= 0 && element.gueris != undefined) {
          myMap3.set(element.date, element.gueris);
        } else if (element.gueris == undefined && element.hospitalises >= 0 && element.hospitalises != undefined) {
          myMap3.set(element.date, 0);
        }
      });
      for (let key of myMap.keys()) {
        tab3.push(key);
      }
      for (let value of myMap.values()) {
        tab2.push(value);
      }
      for (let value of myMap2.values()) {
        tab4.push(value);
      }
      for (let value of myMap3.values()) {
        tab5.push(value);
      }
    })
    console.log(myMap);
    console.log(myMap2);
    console.log(myMap3);
    const xAxisData = tab3;
    const data = tab2;
    const data2 = tab4;
    const data3 = tab5;
    console.log(data);
    console.log(data2);
    console.log(data3);
    this.options = {
      legend: {
        data: ['Hospitalisations', 'Guerisons' ],
        align: 'left',
        textStyle: {
          color: '#d48265'
        }
      },
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: 'Hospitalisations',
          type: 'bar',
          data: data,
          animationDelay: (idx) => idx * 10,
        },
        {
          name: 'Guerisons',
          type: 'bar',
          data: data3,
          animationDelay: (idx) => idx * 10,
        }

      ],
      color: ['#61a0a8', '#bda29a'],
      textStyle: {
        color: '#ffffff'

      },
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };

  }


  changeEvent(event) {
    this.date = event.value;
    const format = 'yyyy-MM-dd';
    const myDate = this.date;
    const locale = 'en-US';
    let dateVeille;
    const formattedDate = formatDate(myDate, format, locale);
    let newDate = new Date(this.date);
    let dateCompare = new Date();
    console.log(dateCompare.getDate() + " " + (dateCompare.getMonth() + 1) + " " + dateCompare.getFullYear());
    if ((newDate.getDate() < 18 && (newDate.getMonth() + 1) <= 3 && newDate.getFullYear() <= 2020) || (newDate.getFullYear() <= 2019)) {
      this.router.navigate(['/error'])

    } else if (newDate > dateCompare) {
      this.router.navigate(['/error'])
    }

    console.log(newDate);
    if (newDate.getDate() < 10 && (newDate.getMonth() + 1) < 10) {
      if (newDate.getDate() == 1 && ((newDate.getMonth() + 1) % 2 == 0 && newDate.getFullYear() % 400 != 0 && (newDate.getMonth() + 1) <= 8) || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 9) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '31';
        console.log("1");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 5 || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 7) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '30';
        console.log("2");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 1) {
        dateVeille =
          (newDate.getFullYear() - 1) +
          '-' + '12' + '-' +
          '31';
        console.log("3");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 3) {
        if (newDate.getFullYear() % 400 != 0) {
          dateVeille =
            newDate.getFullYear() +
            '-' + '0' +
            newDate.getMonth() +
            '-' + '29';
          console.log("4");
        } else {
          dateVeille =
            newDate.getFullYear() +
            '-' + '0' +
            newDate.getMonth() +
            '-' + '28';
          console.log("5");
        }
      } else if (newDate.getDate() != 1) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          (newDate.getMonth() + 1) +
          '-' + '0' +
          (newDate.getDate() - 1);
        console.log("6");
      }
      console.log("7");
    } else if (newDate.getDate() > 10 && (newDate.getMonth() + 1) >= 10) {
      dateVeille =
        newDate.getFullYear() +
        '-' +
        (newDate.getMonth() + 1) +
        '-' +
        (newDate.getDate() - 1);
      console.log("8");
    } else if (newDate.getDate() == 10 && (newDate.getMonth() + 1) >= 10) {
      dateVeille =
        newDate.getFullYear() +
        '-' +
        (newDate.getMonth() + 1) +
        '-' + '0' +
        (newDate.getDate() - 1);
      console.log("88");
    } else if (newDate.getDate() < 10 && (newDate.getMonth() + 1) >= 10) {

      if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 11) {
        dateVeille =
          newDate.getFullYear() +
          '-' +
          newDate.getMonth() +
          '-' + '31';
        console.log("9");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) % 2 == 0 && (newDate.getMonth() + 1) != 12) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '30';
        console.log("10");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) % 2 == 0 && (newDate.getMonth() + 1) == 12) {
        dateVeille =
          newDate.getFullYear() +
          '-' +
          newDate.getMonth() +
          '-' + '30';
        console.log("11");
      } else if (newDate.getDate() != 1) {
        dateVeille =
          newDate.getFullYear() +
          '-' +
          (newDate.getMonth() + 1) +
          '-' + '0' +
          (newDate.getDate() - 1);
        console.log("15");
      }
    } else if (newDate.getDate() >= 10 && (newDate.getMonth() + 1) < 10) {

      if (newDate.getDate() == 1 && ((newDate.getMonth() + 1) % 2 == 0 && newDate.getFullYear() % 400 != 0 && (newDate.getMonth() + 1) <= 8) || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 9) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '31';
        console.log("16");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) % 3 == 0 || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 5 || newDate.getDate() == 1 && (newDate.getMonth() + 1) == 7) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          newDate.getMonth() +
          '-' + '30';
        console.log("17");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 1) {
        dateVeille =
          (newDate.getFullYear() - 1) +
          '-' + '12' + '-' +
          '31';
        console.log("18");
      } else if (newDate.getDate() == 1 && (newDate.getMonth() + 1) == 3) {
        if (newDate.getFullYear() % 400 != 0) {
          dateVeille =
            newDate.getFullYear() +
            '-' + '0' +
            newDate.getMonth() +
            '-' + '28';
          console.log("19");
        } else {
          dateVeille =
            newDate.getFullYear() +
            '-' + '0' +
            newDate.getMonth() +
            '-' + '29';
          console.log("20");
        }
      } else if (newDate.getDate() != 10) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          (newDate.getMonth() + 1) +
          '-' +
          (newDate.getDate() - 1);
        console.log("21");
      }
      else if (newDate.getDate() == 10) {
        dateVeille =
          newDate.getFullYear() +
          '-' + '0' +
          (newDate.getMonth() + 1) +
          '-' + '0' +
          (newDate.getDate() - 1);
        console.log("22");
      }
      console.log("23");
    }
    this.serviceResultat.get2ResultatByDate(formattedDate, dateVeille).subscribe((res: any[]) => {

      this.resultatRegion = res[0].allFranceDataByDate;
      this.resultatRegionVeille = res[1].allFranceDataByDate;

       if(this.nomRegion == 1){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Paris" || x.nom == "Hauts-de-Seine" || x.nom == "Seine-Saint-Denis" || x.nom == "Val-de-Marne" || x.nom == "Val-d'Oise" || x.nom == "Essonne"
          || x.nom == "Yvelines" || x.nom == "Seine-et-Marne");
     
        }
        else if(this.nomRegion== 2){
          
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Auvergne" || x.nom == "Ain" || x.nom == "Allier" || x.nom == "Ardèche" || x.nom == "Cantal" || x.nom == "Drôme"
          || x.nom == "Isère" || x.nom == "Loire" ||  x.nom == "Haute-Loire" || x.nom == "Puy-de-Dôme" || x.nom == "Rhône" || x.nom == "Savoie" || x.nom == "Haute-Savoie");
        
        }
       else if(this.nomRegion== 3){
     
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Aisne" || x.nom == "Nord" || x.nom == "Oise" || x.nom == "Pas-de-Calais" || x.nom == "Somme");

         
        }
       else if(this.nomRegion == 4){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Alpes-de-Haute-Provence" || x.nom == "Hautes-Alpes" || x.nom == "Alpes-Maritimes" || x.nom == "Bouches-du-Rhône" || x.nom == "Var" || x.nom == "Vaucluse");
          
        }
       else if(this.nomRegion == 5){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Ariège" || x.nom == "Aude" || x.nom == "Aveyron" || x.nom == "Gard" || x.nom == "Haute-Garonne" || x.nom == "Gers" );
         
        }
     else if(this.nomRegion == 6){
       
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Charente" || x.nom == "Charente-Maritime" || x.nom == "Corrèze" || x.nom == "Creuse" || x.nom == "Dordogne" || x.nom == "Gironde" 
          || x.nom == "Landes" || x.nom == "Lot-et-Garonne" || x.nom == "Pyrénées-Atlantiques" || x.nom == "Deux-Sèvres" || x.nom == "Vienne" || x.nom == "Haute-Vienne");
        
        }
       else if(this.nomRegion == 7){
       
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Ardennes" || x.nom == "Aube" || x.nom == "Marne" || x.nom == "Haute-Marne" || x.nom == "Meurthe-et-Moselle" || x.nom == "Meuse" 
          || x.nom == "Moselle" || x.nom == "Bas-Rhin" || x.nom == "Haut-Rhin" || x.nom == "Haut-Rhin" || x.nom == "Vienne" || x.nom == "Vosges");
          
        }
       else if(this.nomRegion == 8){
        
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Calvados" || x.nom == "Eure" || x.nom == "Manche" || x.nom == "Orne" || x.nom == "Seine-Maritime");
        
        }
      else if(this.nomRegion == 9){
         
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Côte-d'Or" || x.nom == "Doubs" || x.nom == "Jura" || x.nom == "Nièvre" || x.nom == "Haute-Saône" || x.nom == "Saône-et-Loire" 
          || x.nom == "Yonne" || x.nom == "Territoire de Belfort");
    
        }
      else if(this.nomRegion == 10){
      
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Loire-Atlantique" || x.nom == "Maine-et-Loire" || x.nom == "Mayenne" || x.nom == "Sarthe" || x.nom == "Vendée" );
       
        }
       else if(this.nomRegion == 11){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Côtes-d'Armor" || x.nom == "Finistère" || x.nom == "Ille-et-Vilaine" || x.nom == "Morbihan");
      
        }
       else if(this.nomRegion == 12){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Haute-Corse" || x.nom == "Corse-du-Sud");
         
        }
       else if(this.nomRegion == 13){
          this.resultatDep = this.resultatRegion.filter(x => x.nom == "Cher" || x.nom == "Eure-et-Loir" || x.nom == "Indre" || x.nom == "Indre-et-Loire" || x.nom == "Loir-et-Cher" || x.nom == "Loiret");
       
        }
      
        this.rows = this.resultatDep;
        this.temp = [...this.resultatDep];
        this.resultatRegionRVeille = this.resultatRegionVeille.find(
          x => x.nom == this.region
        );
        this.resultatRegionR = this.resultatRegion.find(
          x => x.nom == this.region
        );

        this.resultatRegionModif = this.resultatRegion.find(x => x.nom == this.region && x.source.nom == 'OpenCOVID19-fr');
     
        this.resultatRegionVeilleModif = this.resultatRegionVeille.find(x => x.nom == this.region && x.source.nom == 'OpenCOVID19-fr');


      if (this.resultatRegionR.hospitalises != undefined && this.resultatRegionR.nouvellesHospitalisations != undefined) {
        this.hospitalisation = this.resultatRegionR.hospitalises;
        this.nouvellesHospitalisation = this.resultatRegionR.nouvellesHospitalisations;
      } else if (this.resultatRegionR.hospitalises != undefined && this.resultatRegionR.nouvellesHospitalisations == undefined) {
        this.hospitalisation = this.resultatRegionR.hospitalises;
        this.nouvellesHospitalisation = this.resultatRegionModif.nouvellesHospitalisations;
      } else if (this.resultatRegionR.hospitalises == undefined && this.resultatRegionR.nouvellesHospitalisations != undefined) {
        this.hospitalisation = this.resultatRegionModif.hospitalises;
        this.nouvellesHospitalisation = this.resultatRegionR.nouvellesHospitalisations;
      } else {
        this.hospitalisation = this.resultatRegionModif.hospitalises;
        this.nouvellesHospitalisation = this.resultatRegionModif.nouvellesHospitalisations;
      }

      // if (this.resultatRegionRVeille.hospitalises != undefined && this.resultatRegionRVeille.nouvellesHospitalisations != undefined) {
      //   this.hospitalisation = this.resultatRegionRVeille.hospitalises;
      //   this.nouvellesHospitalisation = this.resultatRegionRVeille.nouvellesHospitalisations;
      // } else if (this.resultatRegionRVeille.hospitalises != undefined && this.resultatRegionRVeille.nouvellesHospitalisations == undefined) {
      //   this.hospitalisation = this.resultatRegionR.hospitalises;
      //   this.nouvellesHospitalisation = this.resultatRegionVeilleModif.nouvellesHospitalisations;
      // } else if (this.resultatRegionRVeille.hospitalises == undefined && this.resultatRegionRVeille.nouvellesHospitalisations != undefined) {
      //   this.hospitalisation = this.resultatRegionVeilleModif.hospitalises;
      //   this.nouvellesHospitalisation = this.resultatRegionRVeille.nouvellesHospitalisations;
      // } else {
      //   this.hospitalisation = undefined;
      //   this.nouvellesHospitalisation = undefined;
      // }


      if (this.resultatRegionR.deces != undefined) {
        if (this.resultatRegionR.decesEhpad != undefined) {
          this.decesAvecEhpad = this.resultatRegionR.deces + this.resultatRegionR.decesEhpad;
          console.log(this.decesAvecEhpad);
          this.deces = undefined;
        } else {
          this.deces = this.resultatRegionR.deces;
          this.decesAvecEhpad = undefined;
          console.log(this.deces);
        }
      } else {
        if (this.resultatRegionModif.decesEhpad != undefined) {
          this.decesAvecEhpad = this.resultatRegionModif.deces + this.resultatRegionModif.decesEhpad;
          console.log(this.decesAvecEhpad);
          this.deces = undefined;
        } else {
          this.deces = this.resultatRegionModif.deces;
          this.decesAvecEhpad = undefined;
          console.log(this.deces);
        }
      }


      if (this.resultatRegionRVeille.deces != undefined) {
        if (this.resultatRegionRVeille.decesEhpad != undefined) {
          this.decesVeilleAvecEhpad = this.resultatRegionRVeille.deces + this.resultatRegionRVeille.decesEhpad;
          this.decesVeille = undefined;
        } else {
          this.decesVeille = this.resultatRegionRVeille.deces;
          this.decesVeilleAvecEhpad = undefined;
        }
      } else {
        if (this.resultatRegionVeilleModif.decesEhpad != undefined) {
          this.decesVeilleAvecEhpad = this.resultatRegionVeilleModif.deces + this.resultatRegionVeilleModif.decesEhpad;
          this.decesVeille = undefined;
        } else {
          this.decesVeille = this.resultatRegionVeilleModif.deces;
          this.decesVeilleAvecEhpad = undefined;
        }
      }

      if (this.decesAvecEhpad == undefined || this.decesVeilleAvecEhpad == undefined) {
        this.diffDeces = this.deces - this.decesVeille;

        this.progDeces = ((this.deces - this.decesVeille) / this.decesVeille) * 100;
      } else if (this.decesAvecEhpad != undefined && this.decesVeilleAvecEhpad != undefined) {
        this.diffDeces = this.decesAvecEhpad - this.decesVeilleAvecEhpad;

        this.progDeces = ((this.decesAvecEhpad - this.decesVeilleAvecEhpad) / this.decesVeilleAvecEhpad) * 100;
      }

      if(Number.isNaN(this.diffDeces)){
        this.diffDeces = false;
      }
      if(Number.isNaN(this.progDeces)){
        this.progDeces = false;
      }

      if (this.hospitalisation != undefined && this.nouvellesHospitalisation != undefined) {
        this.progHospi =
          (this.nouvellesHospitalisation /
            (this.hospitalisation -
              this.nouvellesHospitalisation)) *
          100;
      } else { this.progHospi = undefined; }

      this.diffConta = this.resultatRegionR.casConfirmes - this.resultatRegionRVeille.casConfirmes;

      if (this.resultatRegionR.gueris != undefined) {
        this.guerrison = this.resultatRegionR.gueris;
        this.diffGueris = this.resultatRegionR.gueris - this.resultatRegionRVeille.gueris;
      } else if (this.resultatRegionR.gueris == undefined) {
        this.guerrison = this.resultatRegionModif.gueris;
        this.diffGueris = this.resultatRegionModif.gueris - this.resultatRegionVeilleModif.gueris;
      }

      if (this.resultatRegionR.reanimation != undefined) {
        console.log(this.resultatRegionR.reanimation);
        this.reanimations = this.resultatRegionR.reanimation;
        this.nouvellesReanimations = this.resultatRegionR.nouvellesReanimations;
      } else if (this.resultatRegionR.reanimation == undefined && this.resultatRegionModif.reanimation != undefined) {
        console.log(this.resultatRegionR.reanimation);
        this.reanimations = this.resultatRegionModif.reanimation;
        this.nouvellesReanimations = this.resultatRegionModif.nouvellesReanimations;
      }
      else if (this.resultatRegionR.reanimation == undefined && this.resultatRegionRVeille.reanimation == undefined && this.resultatRegionModif.reanimation == undefined && this.resultatRegionVeilleModif.reanimation == undefined) {
        this.reanimations = undefined;
        this.nouvellesReanimations = undefined;
      }
      this.progConta =
        ((this.resultatRegionR.casConfirmes -
          this.resultatRegionRVeille.casConfirmes) /
          this.resultatRegionRVeille.casConfirmes) *
        100;


        if(Number.isNaN(this.diffGueris)){
          this.diffGueris = false;
        }
        if(Number.isNaN(this.diffConta)  || Number.isNaN(this.progConta) ){
          this.diffConta == false;
          this.progConta == false;
        }
        if(Number.isNaN(this.progHospi)){
          this.progHospi == false; 
        }

    });


  }
  navigate() {
    this.router.navigate(['/jour'])
  }
}
