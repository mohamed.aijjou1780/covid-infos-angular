import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Resultat} from '../models/resultat';
import { forkJoin } from 'rxjs';
import { NumberSymbol } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AfficheServiceService {

  constructor(private http : HttpClient) { }



  public getResultatByDate(date: string): Observable<any> {
    return this.http.get<any>('https://coronavirusapi-france.now.sh/AllDataByDate?date=' + date);
  }

 public get2ResultatByDate(date: string, date2: string): Observable<any[]> {

   let response1 = this.http.get<any>('https://coronavirusapi-france.now.sh/AllDataByDate?date=' + date);
   let response2 = this.http.get<any>('https://coronavirusapi-france.now.sh/AllDataByDate?date=' + date2);

    return forkJoin([response1, response2]);
  }

  public getCasConfirmesForFrance():Observable<any>{

return this.http.get<any>("https://coronavirusapi-france.now.sh/AllDataByDepartement?Departement=France");

  }

  public getResultatRegion(region: string): Observable<any>{

    return this.http.get<any>("https://coronavirusapi-france.now.sh/AllDataByDepartement?Departement=" + region);

  }




}
