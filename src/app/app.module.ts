import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { AppComponent } from './app.component';
import { AfficheJourComponent } from './pages/affiche-jour/affiche-jour.component';
import { AfficheSemaineComponent } from './pages/affiche-semaine/affiche-semaine.component';
import { AfficheMoisComponent } from './pages/affiche-mois/affiche-mois.component';
import { NavsideComponent } from './pages/navside/navside.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { 
	IgxDatePickerModule
 } from "igniteui-angular";
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { formatDate } from "@angular/common";
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { NgxEchartsModule } from 'ngx-echarts';
import {MatExpansionModule} from '@angular/material/expansion';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { ErrorComponent } from './pages/error/error.component';
import { RegionComponent } from './pages/region/region.component'; 
import { NgxSpinnerModule } from "ngx-spinner";


@NgModule({
  declarations: [
    AppComponent,
    AfficheJourComponent,
    AfficheSemaineComponent,
    AfficheMoisComponent,
    NavsideComponent,
    ErrorComponent,
    RegionComponent
  ],
  imports: [

    NgxPaginationModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    IgxDatePickerModule,
    MatDatepickerModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatNativeDateModule,
    MatInputModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatPaginatorModule,
    NgxDatatableModule,
    FontAwesomeModule,
    MatProgressSpinnerModule,
     NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),
    MatExpansionModule,
    CommonModule,
    NgxSpinnerModule

  
    
 

 

    

    

    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
